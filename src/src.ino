#include "lcdgfx.h"
#include "lcdgfx_gui.h"
#include <avr/pgmspace.h>
#include <math.h>
#include <EEPROM.h>
#include "animations.h"

#define FRAME_WIDTH 128
#define FRAMW_HEIGHT 64
#define FRAME_PIXEL_COUNT FRAME_WIDTH*FRAMW_HEIGHT

#define LIGHT_SENSOR_PIN A0
#define MOISTURE_SENSOR_PIN A1

#define LEFT_BUTTON_PIN 8
#define CENTER_BUTTON_PIN 9
#define RIGHT_BUTTON_PIN 10

#define BRIGHTNESS_EEPROM 1
#define MOISTURE_BAD_EEPROM 2
#define MOISTURE_GOOD_EEPROM 3

#define FACE_DISPLAY 3
#define SETTINGS_MENU_DISPLAY 4
#define MOISTURE_SETTING_DISPLAY 2
#define DRY_SETTING_DISPLAY 1
#define LIGHT_SETTING_DISPLAY 0

byte currentDisplayState = FACE_DISPLAY;

DisplaySSD1306_128x64_I2C display(-1);

// canvas for double buffering
const int canvasWidth = 128;
const int canvasHeight = 64;
uint8_t canvasData[canvasWidth*(canvasHeight/8)];
NanoCanvas1 canvas(canvasWidth, canvasHeight, canvasData);

Animation *currentAnimation = &Idle;
Animation *animationInPreviousFrame = NULL;
Animation *lastFinishedAnimation = NULL;
byte currentAnimationFrame = 0;
short currentAnimationDataIndex = 0;

byte previousCenterButtonState = 0;

const char *menuItems[] =
{
    "SVETLO",
    "SUCHO",
    "VLHKOST",
    "ZAVRIT",
};

LcdGfxMenu menu( menuItems, sizeof(menuItems) / sizeof(char *));

#define AHOJ 0
#define RUNTIMES 1
#define TESTIMAGE 2

typedef byte (*PercentageCalculator)();

byte IsAnimationFinished()
{
  return pgm_read_byte(&(currentAnimation->frameCount)) == currentAnimationFrame;
}

void SetAnimation(Animation *nextAnimation)
{
  currentAnimation = nextAnimation;

  if (currentAnimation == animationInPreviousFrame)
  {
    return;
  }

  animationInPreviousFrame = currentAnimation;
  currentAnimationFrame = 0;
  currentAnimationDataIndex = 0;
}

void RenderNextAnimationFrame()
{
    byte x = 0, y = 0;
    short pixelsRendered = 0;

    while (pixelsRendered != FRAME_PIXEL_COUNT)
    {
      byte data = pgm_read_byte(&(currentAnimation->data[currentAnimationDataIndex]));
      byte pixelColor = (data & 0b10000000) >> 7;
      byte pixelCount = data & 0b01111111;
      canvas.setColor(pixelColor);

      if (x + pixelCount < FRAME_WIDTH)
      {
        canvas.drawHLine(x,y, x + pixelCount);
        x += pixelCount;

        if (x == FRAME_WIDTH)
        {
          x = 0;
          y += 1;
        }
      }
      else
      {
        canvas.drawHLine(x,y, FRAME_WIDTH);
        byte remainingPixelCount = pixelCount - (FRAME_WIDTH - x);
        y += 1;
        x = remainingPixelCount;
        canvas.drawHLine(0, y, x);
      }

      pixelsRendered += pixelCount; 
      currentAnimationDataIndex++;
    }

    display.drawCanvas(0,0, canvas);
    currentAnimationFrame++;
    delay(100);
}

void InitLightSensor()
{
  pinMode(A0, INPUT);
}

void InitMoistureSensor()
{
  pinMode(A1, INPUT);
}

void InitButtons()
{
  pinMode(LEFT_BUTTON_PIN, INPUT);
  pinMode(RIGHT_BUTTON_PIN, INPUT);
  pinMode(CENTER_BUTTON_PIN, INPUT);
}

void setup()
{
  InitLightSensor();
  InitButtons();
  InitMoistureSensor();

  currentAnimation = &Idle;
  animationInPreviousFrame = &Idle;
  Serial.begin(115200);
  display.begin();
  display.clear();
  
}

byte CenterButtonPressed()
{
  byte currentValueRaw = digitalRead(CENTER_BUTTON_PIN);
  byte currentValue = currentValueRaw != previousCenterButtonState && currentValueRaw; 

  previousCenterButtonState = currentValueRaw;

  return currentValue;
}

byte LeftButtonPressed()
{
  return digitalRead(LEFT_BUTTON_PIN);
}

byte RightButtonPressed()
{
  return digitalRead(RIGHT_BUTTON_PIN);
}

void UpdateDisplay()
{
  display.drawCanvas(0,0,canvas);
}

void SetColorOn()
{
  canvas.setColor( 0xFF );
}

void SetColorOff()
{
  canvas.setColor( 0 );
}

byte GetBrightnessPercentage()
{
  return (byte)((analogRead(LIGHT_SENSOR_PIN) / 1024.0) * 100);
}

byte GetMoisturePercentage()
{
  // in cup of water => about 200
  // in air => about 520
  short rawValue = analogRead(MOISTURE_SENSOR_PIN);
  rawValue = rawValue < 200 ? 200 : rawValue;
  return ((rawValue - 200) / 320.0) * 100; // water in cup offset
}

void SettingsMenu()
{
  canvas.clear();
  display.clear();
  display.setFixedFont(ssd1306xled_font6x8_AB);
  menu.show(display);
  while(1)
  {
      if (LeftButtonPressed())
      {
        menu.up();
        menu.show(display);
        delay(100);
      }
      else if (RightButtonPressed())
      {
        menu.down();
        menu.show(display);
        delay(100);
      }
      else if (CenterButtonPressed())
      {
        currentDisplayState = menu.selection();
        // Serial.println(currentDisplayState);
        return;
      }
  }
}

void SettingsRender(const char* headerText, byte eepromAddress, PercentageCalculator calculatePercentage) {
  display.clear();
  canvas.clear();

  byte triggerValue = EEPROM.read(eepromAddress);

  while(!CenterButtonPressed())
  {
    canvas.setFixedFont( ssd1306xled_font6x8 );
    canvas.clear();

    // Header
    canvas.printFixed(7,  12, "___________________", STYLE_NORMAL);
    canvas.printFixed(7,  8, headerText, STYLE_NORMAL);    

    SetColorOn();
    canvas.drawRect(10, 25, 118, 40);
  
    canvas.fillRect(12, 27, calculatePercentage() + 12, 38);

    if (RightButtonPressed())
    {
      triggerValue++;
      delay(10);
    }
    if (LeftButtonPressed())
    {
      triggerValue--;
      delay(10);
    }

    triggerValue = triggerValue > 100 ? 99 : triggerValue % 100;

    canvas.drawHLine(12 + triggerValue, 48, 14 + triggerValue);
    canvas.drawHLine(10 + triggerValue, 49, 16 + triggerValue);
    canvas.drawHLine(8 + triggerValue, 50, 18 + triggerValue);
    canvas.drawHLine(6 + triggerValue, 51, 20 + triggerValue);

    UpdateDisplay();
  }

  EEPROM.write(eepromAddress, triggerValue);
  canvas.clear();

  canvas.setFixedFont( ssd1306xled_font8x16 );
  canvas.printFixed(30, 20, "Ulozeno!", STYLE_BOLD);
  display.drawCanvas(0,0, canvas);
  delay(1000);

  currentDisplayState = SETTINGS_MENU_DISPLAY;
  display.clear();
  canvas.clear();
}

byte DrynessTriggered()
{
  return EEPROM.read(MOISTURE_BAD_EEPROM) < GetMoisturePercentage();
}

byte MoistureTriggered()
{
  return EEPROM.read(MOISTURE_GOOD_EEPROM) > GetMoisturePercentage();
}

byte DarknessTriggered()
{
  return EEPROM.read(BRIGHTNESS_EEPROM) > GetBrightnessPercentage();
}

void HandleFaceAnimation()
{
  RenderNextAnimationFrame();

  if (IsAnimationFinished())
  {
    currentAnimationFrame = 0;
    currentAnimationDataIndex = 0;
    lastFinishedAnimation = currentAnimation;
  }

  // ANGRY START
  if (DrynessTriggered())
  {
    SetAnimation(&Angry);
    return;
  }
  else
  {
    if (lastFinishedAnimation == &Angry)
    {
      SetAnimation(&AngryEnd);
      return;
    }
  }
  // ANGRY END

  // HAPPY START
  if (MoistureTriggered())
  {
    if (lastFinishedAnimation == &HappyStart || lastFinishedAnimation == &Happy)
    {
      SetAnimation(&Happy);
    }
    else
    {
      SetAnimation(&HappyStart);
    }
    
    return;
  }
  else
  {
    if (lastFinishedAnimation == &HappyStart || lastFinishedAnimation == &Happy)
    {
      SetAnimation(&HappyEnd);
      return;
    }
  }
  // HAPPY END

  // SLEEP START
  if (DarknessTriggered())
  {
    if (lastFinishedAnimation == &SleepStart || lastFinishedAnimation == &Sleep)
    {
      SetAnimation(&Sleep);
    }
    else
    {
      SetAnimation(&SleepStart);
    }

    return;
  }
  else
  {
    if (lastFinishedAnimation == &SleepStart || lastFinishedAnimation == &Sleep)
    {
      SetAnimation(&SleepEnd);
      return;
    }
  }
  // SLEEP END

  SetAnimation(&Idle);
}

void RenderCurrentDisplaySetting()
{
  switch (currentDisplayState)
  {
    case FACE_DISPLAY:

      HandleFaceAnimation();

      if (CenterButtonPressed())
      {
        delay(500);
        currentDisplayState = SETTINGS_MENU_DISPLAY;
      }
      break;
    case SETTINGS_MENU_DISPLAY:
      SettingsMenu();
      break;
    case MOISTURE_SETTING_DISPLAY:
      SettingsRender("Citlivost na vlhkost", MOISTURE_GOOD_EEPROM, GetMoisturePercentage);
      break;
    case DRY_SETTING_DISPLAY:
      SettingsRender("Citlivost na sucho", MOISTURE_BAD_EEPROM, GetMoisturePercentage);
      break;
    case LIGHT_SETTING_DISPLAY:
      SettingsRender("Citlivost na svetlo", BRIGHTNESS_EEPROM, GetBrightnessPercentage);
      break;
  }
}

void loop()
{
  RenderCurrentDisplaySetting();
}