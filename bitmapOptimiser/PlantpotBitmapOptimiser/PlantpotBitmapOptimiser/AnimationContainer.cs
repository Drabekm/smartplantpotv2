﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantpotBitmapOptimiser
{
    public class AnimationContainer
    {
        private const string _structName = "Animation";

        public string Name { get; set; } = "";
        public int FrameCount { get; set; }
        public int Delay { get; set; } = 500;
        public List<ComprimationPair> Comprimationpairs { get; set; } = new List<ComprimationPair>();

        public override string ToString()
        {
            return $@"const {_structName} {Name} PROGMEM =
{{
    {FrameCount},
    {Delay},
    {{{string.Join(',', Comprimationpairs.Select(x => x.ToString()))}}}
}};";
        }
    }
}
