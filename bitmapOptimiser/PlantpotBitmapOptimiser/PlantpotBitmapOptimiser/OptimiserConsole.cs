﻿using PlantpotBitmapOptimiser;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MyProject;
class OptimiserConsole
{
    static void Main(string[] args)
    {
        var currentDirrectoryPath = Directory.GetCurrentDirectory();
        var directories = Directory.GetDirectories(currentDirrectoryPath);

        var animationContainers = new List<AnimationContainer>();

        StringBuilder output = new StringBuilder();

        foreach(var directory in directories)
        {
            Console.WriteLine($"Processing files in {directory}");

            var imagePaths = Directory.GetFiles(directory).Where(x => x.EndsWith(".bmp")).ToList();

            var comprimationPairs = new List<ComprimationPair>();
            var currentAnimationContainer = new AnimationContainer
            {
                Name = directory.Split("\\").Last(),
                FrameCount = imagePaths.Count(),
                Comprimationpairs = comprimationPairs
            };

            imagePaths = imagePaths.OrderBy(x => int.Parse(Regex.Replace(x.Split("\\").Last(), @"[^\d]", ""))).ToList();
            foreach (var imagePath in imagePaths)
            {
                var bitmap = new Bitmap(imagePath);
                var currentPair = new ComprimationPair();

                currentPair.PixelValue = currentPair.ColorToPixelValue(bitmap.GetPixel(0, 0));

                for (int y = 0; y < bitmap.Height; y++)
                {
                    for (int x = 0; x < bitmap.Width; x++)
                    {
                        if (currentPair.PixelHasSameValue(bitmap.GetPixel(x, y)))
                        {
                            currentPair.PixelCount++;
                        }
                        else
                        {
                            comprimationPairs.Add(currentPair);
                            currentPair = new ComprimationPair
                            {
                                PixelCount = 1,
                                PixelValue = currentPair.ColorToPixelValue(bitmap.GetPixel(x, y))
                            };
                        }

                        if (currentPair.PixelCount == ComprimationPair.MaxPixelCount)
                        {
                            comprimationPairs.Add(currentPair);

                            var nextX = x == bitmap.Width - 1 ? 0 : x + 1;
                            var nextY = x == bitmap.Width - 1 ? y + 1 : y;

                            currentPair = new ComprimationPair
                            {
                                PixelCount = 0,
                                PixelValue = currentPair.ColorToPixelValue(bitmap.GetPixel(nextX, nextY))
                            };
                        }
                    }
                }

                if (currentPair.PixelCount > 0)
                {
                    comprimationPairs.Add(currentPair);
                }
            }

            animationContainers.Add(currentAnimationContainer);
        }

        //animationContainers.OrderBy(x => int.Parse(Regex.Replace(x.Name, @"[^\d]", "")));

        output.AppendLine(@"struct Animation
{
            const byte frameCount;
            const byte delay;
            const byte data[];
        }; ");
        output.AppendLine(string.Join("\n", animationContainers.Select(x => x.ToString())));
        output.AppendLine($"const Animation Animations[] PROGMEM = {{{string.Join(",", animationContainers.Select(x => x.Name))}}};");
        output.AppendLine(string.Join("\n", animationContainers.Select((x, i) => $"#define {x.Name.ToUpper()} {i}")));

        using (StreamWriter sw = new StreamWriter("animations.h"))
        {
            sw.Write(output.ToString());
        }
    }
}