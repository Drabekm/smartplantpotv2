﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantpotBitmapOptimiser
{
    public class ComprimationPair
    {
        public static int MaxPixelCount = 127;
        public int PixelValue { get; set; }
        public int PixelCount { get; set; }

        public int ColorToPixelValue(Color color)
        {
            return color.GetBrightness() > 0 ? 1 : 0;
        }

        public bool PixelHasSameValue(Color color)
        {
            return ColorToPixelValue(color) == PixelValue;
        }

        public override string ToString()
        {
            var finalValue = PixelValue << 7 | PixelCount;

            return $"0x{finalValue.ToString("x")}";
        }
    }
}
